# Llamadas INER

## Descripción

Este repositorio contiene información sobre las llamadas telefónicas contestadas en el módulo de neumología adultos de consulta externa del INER. Solo contiene información útil con fines estadísticos.

## Procedimiento de atención telefónica.

1. Indicar que llama al módulo de consulta externa
2. Preguntar que trámite desea hacer:
   - Agendar una cita
   - Reagendar una cita
   - Cancelar una cita
   - Otro
3. En los primeros tres casos, se solicita el número de expediente; En otro caso se identifica el trámite y se continúa o se salta al paso 10
4. Se pregunta el lugar de residencia del paciente
5. Se busca el expediente electrónico del paciente, se anota el sexo y la edad del paciente
6. El paciente indica la especialidad de su cita
7. Si es cita de primera vez, se verifica en el expediente electrónico evidencia de la interconsulta y se procede al paso 9
8. Si es consulta subsecuente, se verifica que el paciente no ha sido dado de alta
9. Se busca el espacio más cercano y acorde a las instrucciones escritas en el expediente (tiempo entre citas principalmente)
10. Si se llega a este punto desde el paso 3 se identifica alguna de estas situaciones:
    - Solicitud de extensión
    - Solicitud de cita
    - Dudas sobre la consulta
    - Otro
11. Se escriben las notas adicionales con los detalles de la llamada
12. Se despide la llamada 

### TODO

- [ ] El paso 6 debería ajustarse al paso 2, de esta forma se ahorraría tiempo para saber si la llamada puede ser atendida en esta extensión
- [ ] Estandarizar lo considerado en los casos del paso 10
- [ ] Estandarizar formas de elaborar las notas adicionales

## Descripción de los datos registrados

Existen dos formularios:

1. Llamadas. Guarda los siguientes datos:
    + Número de expediente
    + Sexo
    + Edad
    + Lugar de residencia
    + Tipo de trámite
    + Especialidad
    + Tipo de cita
    + ¿El usuario tuvo el resultado deseado?
    + Notas adicionales
2. Llamadas no correspondientes
    + ¿Por qué no se atendió?
    + Notas adicionales
    
## Uso del bot de análisis de datos

1. Dar click en el siguiente botón: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/genomorro%2Finer/HEAD)
2. Se abrirá otra pestaña en el navegador, esperar a que cargue la página, puede tardar en completar dicho proceso
3. Se abrirá el archivo `Preprocesado.ipynb` y se presiona el botón `Restart Kernel and Run All Cells`
4. Se abrirá el archivo `main.ipynb` y se identificará la siguiente estructura:
   - Chatbot para el análisis básico de DS con PandasAI
   - Agregar los datasets a analizar
     + Analizar un dataset a la vez
     + Analizar datasets en conjunto
     + Analizar por medio de un agente
   - Preguntar
     + Un dataframe a la vez
     + Preguntar al conjunto de dataframes
     + Preguntar a un agente
5. Se debe ejecutar cada celda de los primeros dos apartados `Chatbot para el análisis básico de DS con PandasAI` y `Agregar los datasets a analizar` (con todas sus subsecciones)
6. En la sección de `Preguntar` hay ejemplos de cómo hacer preguntas al bot, considerando los datos de los formularios por separado o juntos. Se pueden modificar las celdas ya existentes con nuevas preguntas, o bien, 
crear celdas nuevas con preguntas diferentes

## Licencia

[Llamadas INER](https://gitlab.com/genomorro/iner) está licenciado bajo [Attribution-NonCommercial 4.0 International![](https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1)![](https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1)![](https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1)](http://creativecommons.org/licenses/by-nc/4.0/?ref=chooser-v1)
